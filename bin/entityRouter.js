// Packages required consts
const express = require('express');
const expressValidator = require('express-validator');
const validator = require('validator');
const { body, validationResult } = require('express-validator/check');
const router = express.Router();

// Data consts
const dataFormat = "json";
const dataPath = "../data/";
const entities = ["factories", "bottles", "contacts", "orders", "stores", "types"];
const data = {};

// Fill data
entities.forEach((entity) => {
    let path = dataPath+entity+"."+dataFormat;
    data[entity] = require(path);
});

// Id generator for new entity objects

function* generateIds(initial) {
    while (true) {
        yield initial++;
    }
}
//let idGenerator = generateIds(factories.length + 1);

/*************** Middlewares and routes for /entity ***************/

router.get('/', (req, res, next) => {
    console.log(req.originalUrl.split('/')[1]);
    res.end();
});

/*******************************************/

module.exports = router;