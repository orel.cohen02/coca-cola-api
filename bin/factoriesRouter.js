const express = require('express');
const expressValidator = require('express-validator');
const validator = require('validator');
const { body, validationResult } = require('express-validator/check');
const router = express.Router();

const path = "../data/factories.json";
let factories = require(path);

function* generateIds(initial) {
    while (true) {
        yield initial++;
    }
}

let idGenerator = generateIds(factories.length + 1);

// For post '/' and put '/:id' purposes
const factoryValidation = [(req, res, next) => {

    if (req.body == null || (typeof req.body) !== 'object' || Array.isArray(req.body)) {

        res.status(400).send("Request body must be a factory object");
    } else {
        next();
    }

}, (req, res, next) => {

    let factoryValidation = validateFactoryObject(req.body);

    if (!(JSON.stringify(factoryValidation) === "{}")) {
        factoryValidation.location = `body [${req.body.indexOf(factory)}]`;
        req.body.error = factoryValidation;
    }

    next();

}, (req, res, next) => {

    if (!req.body.error) {
        next();
    } else {
        res.status(400).send(req.body.error);
    }
}];

// For put '/' purposes
const ArrayValidation = [
    //Checks if the body is even an array
    (req, res, next) => {

        if (!Array.isArray(req.body)) {
            res.status(400).send('Must send an array of factories.');
        } else {
            next();
        }
    },
    // Validating all the objects in the array
    (req, res, next) => {

        req.body.errors = {};

        req.body.forEach((factory) => {
            let factoryValidation = validateFactoryObject(factory);
            if (!(JSON.stringify(factoryValidation) === "{}")) {
                factoryValidation.location = `body [${req.body.indexOf(factory)}]`;
                req.body.errors[req.body.indexOf(factory)] = factoryValidation;
            }
        });

        next();
    },
    // Sending errors if exist
    (req, res, next) => {
        if (JSON.stringify(req.body.errors) === "{}") {
            next();
        } else {
            res.status(400).send(req.body.errors);
        }
}];

// Validate a single *factory* and return object with 
function validateFactoryObject(factory) {

    let err = {};

    let validationParameters = [{
        property: "name",
        regex: /^[A-Za-z ]+$/,
        msg: "Field must be defined and a non-empty string with no numbers"
    },
    {
        property: "address",
        regex: /[A-Za-z]+[A-Za-z0-9 ]*/,
        msg: "Field must be defined and a non-empty string"
    }];

    validationParameters.forEach((parameter) => {
        //validationResults.push(
        let validationResult = validateProperty(factory, parameter.property, parameter.regex, parameter.msg);
        if (validationResult) {
            err[parameter.property] = validationResult;
        }
    });

    return err;
}

// Validate a single *property* of a factory
function validateProperty(factory, property, regex, message) {

    if (!((factory[property]) && (factory[property]).toString().match(regex))) {
        return {
            "param": property,
            "msg": message
        };
    }
}

function newFactory(request) {
    return {
        id: idGenerator.next().value,
        address: request.body.address,
        name: request.body.name
    };
}

/**************** path '/' - treats all factories ****************/

router.get('/', (req, res) => {
    res.status(200).send(factories);
});

router.post('/', factoryValidation, (req, res) => {

    // TODO: Function that creates factory
    let factory = newFactory(req);
    factories.push(factory);
    res.status(200).send(`Added new factory with id ${factory.id}`);
});

router.put('/', ArrayValidation, (req, res, next) => {

    // TODO: Add check of body 
    let newArray = [];
    idGenerator = generateIds(1);
    req.body.forEach((factory) => {
        newArray.push({
            id: idGenerator.next().value,
            address: factory.address,
            name: factory.name
        })
    });
    factories = newArray;
    res.status(200).send("All factories were successfully updated");
})

router.delete('/', (req, res) => {

    idGenerator = generateIds(1);

    factories = [];
    res.status(200).send('All factories were deleted.');
})

/**************** path '/:id' - treats certain factory ****************/

// Handle NO ID FOUND error
router.use('/:id', (req, res, next) => {
    if (!(factories.find(x => x.id == req.params.id))) {
        res.status(404)
            .send('No factory with this id');
    } else {
        next();
    }
})

router.get("/:id", (req, res) => {
    let factory = factories.find(x => x.id == req.params.id);
    res.status(200).send(factory);
});

router.post("/:id", (req, res) => {
    res.status(405)
        .send('Method not allowed. Use /factories to post a new factory');
});

router.put("/:id", factoryValidation, (req, res) => {

    factories[factories.indexOf(factories.find(elem => elem.id == req.params.id))]
        = {
            id: req.params.id,
            address: req.body.address,
            name: req.body.name
        };
    res.status(200).send(`Factory with id ${req.params.id} was successfully updated!`);
});

router.delete("/:id", (req, res) => {

    factories = factories.filter(elem => elem.id != req.params.id);
    res.status(200).send(`factory with id ${req.params.id} was deleted`);
});

/*******************************************/

module.exports = router;