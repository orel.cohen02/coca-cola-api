"use strict"

// Express main app consts
const express = require('express');
const app = express();
const port = 3000;

const httpStatusCodes = require("http-status-codes");

// Middleware to parse body before each request
const bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.use(function (err, req, res, next) {
    if (err instanceof SyntaxError) {
        res.status(httpStatusCodes.BAD_REQUEST)
            .send("Invalid JSON syntax.");
    }
});

// TODO: Remove
const paths = {
    bottles:"../data/bottles.json",
    orders:"../data/orders.json",
    stores:"../data/stores.json"
}

// Router consts
const apiRouter = express.Router();
const factoriesRouter = require('./factoriesRouter.js');
const typesRouter = require('./typesRouter.js');
const contactsRouter = require('./contactsRouter.js')


// Router uses
apiRouter.use('/factories', factoriesRouter);
apiRouter.use('/types', typesRouter);
apiRouter.use('/contacts', contactsRouter);

app.use('/api/v1', apiRouter);


app.listen(port, () => console.log(`Server is listening on port ${port}`));