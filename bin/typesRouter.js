const express = require('express');
const expressValidator = require('express-validator');
const validator = require('validator');
const {body, validationResult } = require('express-validator/check');
const router = express.Router();

const path = "../data/types.json";
let types = require(path);

function* generateIds(initial) {
  while(true) {
    yield initial++;
  }
}
 let idGenerator = generateIds(types.length + 1);

const typeValidation = [body(['volume', 'price'])
                                .matches(/^(\d+\.*\d*?)$/)
                                .withMessage("Field must be defined and a number"),
                           (req, res, next) => {

                                const errors = validationResult(req);
                                
                                if(!errors.isEmpty()) {

                                    res.status(400).json({ errors: errors.mapped() }).end();
                                } else {
                                    next();
                                }
                           }
];

/*const ArrayValidation = [(req, res, next) => {

    if(!Array.isArray(req.body)) {
        res.status(400).send('Must send an array of types.');
    } else {
        next();
    }
},

    (req, res, next) => {
        
        req.forEach((type) => {
            check
        });
}]*/

function newType(request) {
    return {id: idGenerator.next().value, 
            volume: Number(request.body.volume), 
            price: Number(request.body.price)};
}

/**************** path '/' - treats all types ****************/

router.get('/', (req, res) => {
        res.status(200).send(types);
});

router.post('/', typeValidation,(req, res) => {

        // TODO: Function that creates type

        let type = newType(req);
        types.push(type);
        res.status(200).send(`New type with id ${type.id} was added successfully!`);
});

router.put('/', typeValidation,(req, res, next) => {

        types = req.body;
})

router.delete('/', (req, res) => {

        idGenerator = generateIds(1);

        types = [];
        res.status(200).end("All types were successfully deleted");
})

/**************** path '/:id' - treats certain type ****************/

// Handle NO ID FOUND error
router.use('/:id', (req, res, next) => {
    if(!(types.find(x => x.id == req.params.id))) {
        res.status(404)
           .send('No type with this id');
    } else {
        next();
    }
})

router.get("/:id" , (req, res) => {
        let type = types.find(x => x.id == req.params.id);
         res.status(200).send(type);
});

router.post("/:id", (req, res) => {
        res.status(405)
           .send('Method not allowed. Use /types to add a new type');
});

router.put("/:id", typeValidation, (req, res) => {

        types[types.indexOf(types.find(elem => elem.id == req.params.id))]
                                = {id : Number(req.params.id),
                                   volume : Number(req.body.volume),
                                   price : Number(req.body.price)};
        res.status(200).end(`Type with id ${req.params.id} was successfully updated`);
});

router.delete("/:id", (req, res) => {

        types = types.filter(elem => elem.id != req.params.id);
        res.status(200).send(`Type with id ${req.params.id}`);
});

/*******************************************/

module.exports = router;