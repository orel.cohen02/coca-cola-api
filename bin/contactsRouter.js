const express = require('express');
const expressValidator = require('express-validator');
const validator = require('validator');
const {body, validationResult } = require('express-validator/check');
const router = express.Router();

const path = "../data/contacts.json";
let contacts = require(path);

// TODO: add yielding ids
function* generateIds(initial) {
  while(true) {
    yield initial++;
  }
}
 let idGenerator = generateIds(contacts.length + 1);


const contactValidation = [body('name')
                                .matches(/^[A-Za-z ]+$/)
                                .withMessage("Field must be defined and a non-empty string with no numbers"),
                            body('address')
                                .matches(/^[A-Za-z0-9 ]+$/)
                                .withMessage("Field must be defined and a non-empty string"),
                           body('phone')
                                .isNumeric()
                                .withMessage("Field must be defined and a phone-number"),
                                
                           (req, res, next) => {

                                const errors = validationResult(req);
                                
                                if(!errors.isEmpty()) {

                                    res.status(400).json({ errors: errors.mapped() }).end();
                                } else {
                                    next();
                                }
                           }
];

/*const ArrayValidation = [(req, res, next) => {

    if(!Array.isArray(req.body)) {
        res.status(400).send('Must send an array of contacts.');
    } else {
        next();
    }
},

    (req, res, next) => {
        
        req.forEach((contact) => {
            check
        });
}]*/

function newContact(request) {
    return {id: idGenerator.next().value, 
            name: request.body.name, 
            address:request.body.address,
            phone : request.body.phone};
}

/**************** path '/' - treats all contacts ****************/

router.get('/', (req, res) => {
        res.status(200).send(contacts);
});

router.post('/', contactValidation,(req, res) => {

        let contact = newContact(req);
        contacts.push(contact);
        res.status(200).send(`New contact with id ${contact.id} was successfully added`);
});

router.put('/', contactValidation,(req, res, next) => {

        // TODO: Add check of body 

        contacts = req.body;
})

router.delete('/', (req, res) => {

        idGenerator = generateIds(1);

        contacts = [];
        res.status(200).end("All contacts were successfully deleted");
})

/**************** path '/:id' - treats certain contact ****************/

// Handle NO ID FOUND error
router.use('/:id', (req, res, next) => {
    if(!(contacts.find(x => x.id == req.params.id))) {
        res.status(404)
           .send('No contact with this id');
    } else {
        next();
    }
})

router.get("/:id" , (req, res) => {
        let contact = contacts.find(x => x.id == req.params.id);
         res.status(200).send(contact);
});

router.post("/:id", (req, res) => {
        res.status(405)
           .send('Method not allowed. Use /contacts to add a new contact');
});

router.put("/:id", contactValidation, (req, res) => {

        contacts[contacts.indexOf(contacts.find(elem => elem.id == req.params.id))]
                                = {
                                    id: req.params.id,
                                    name: req.body.name,
                                    address: req.body.address,
                                    phone: req.body.phone
                                };
        res.status(200).send(`Contact with id ${req.params.id} was successfully updated`);
});

router.delete("/:id", (req, res) => {

        contacts = contacts.filter(elem => elem.id != req.params.id);
        res.status(200).end(`Contact with id ${req.params.id} was successfully deleted`);
});

/*******************************************/

module.exports = router;