// entities is only in order to know the entity fields,
// Once we move to db we'll kmow them already so we wot need to require it
const { data, entities } = require('./config.js');

module.exports = class dal {

    addObject(entity, object) {
        data[entity].push(object);
    }

    getObject(entity, id) {
        return (data[entity]).find(x => x.id == id);
    }

    getData(entity) {
        return data[entity];
    }

    updateObject(entity, id, updates) {

        let updatedObj = data[entity].find(x => x.id == id);
        for (let property in entities[entity]) {
            if (updates[property] !== undefined) {
                updatedObj[property] = updates[property];
            }
        }
    }

    deleteObject(entity, id) {
        data[entity] = data[entity].filter(elem => elem.id != id);
    }

    deleteEntityData(entity) {
        data[entity] = [];
    }
}