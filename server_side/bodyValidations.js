const { entities, data } = require('./config.js');
const httpStatusCodes = require("http-status-codes");


function validateIdAvailable (req, res, next) {
    let entityArray = data[req.entity];
    if (!(entityArray.find(x => x.id == req.params.id))) {
        res.status(404)
            .send(`No ${req.entity} found with this id`);
    } else {
        next();
    }
};

// For PUT /entity/:id only
function bodyNotArray(req, res, next) {
    if (Array.isArray(req.body)) {
        res.status(httpStatusCodes.BAD_REQUEST).send(`Body must be a single object of type ${req.entity}`)
    } else {
        next();
    }
}


// Insert body as object to array and handle
function bodyMatchesValidators(req, res, next) {
    if (!Array.isArray(req.body)) {
        req.body = ([]).concat(req.body);
    }

    validateArrayBody(req, res, next);
}


// Validate an array

function validateArrayBody(req, res, next) {

    let errors = {};
    req.body.forEach((obj) => {

        // Element is not object - send error, element doesn't match 
        errors[req.body.indexOf(obj)] =
            ((typeof obj !== 'object') || Array.isArray(obj)) ?

                `Property must be a valid object of type ${req.entity}`
                : (JSON.stringify(singleObjectValidationErrors(obj, req)) !== '{}') ?
                    singleObjectValidationErrors(obj, req) : undefined
    })

    JSON.stringify(errors) == '{}' ? next() : res.status(httpStatusCodes.BAD_REQUEST).send(errors);
}

// Get a single object and it's entity name and return the error object for it
function singleObjectValidationErrors(object, req) {

    let errors = {};

    // If method is put and url is /entity (not /entity/:id) 
    // Then check id first
    if (req.method === 'PUT' && req.url === '/' + req.entity) {
        if (typeof object.id === 'undefined') {

            // Object id not defined
            errors.id = 'Object id must be defined.';

        } else if (!((data[req.entity]).find(x => x.id == object.id))) {

            // No object with this id
            errors.id = `No ${req.entity} found with an id of ${object.id}.`;
        }
    }

    for (let attr in entities[req.entity]) {

        if (typeof object[attr] === "undefined") {

            // Add an error message - attribute should be defined
            if (req.method === 'POST') {
                errors[attr] = `Property ${attr} must be defined.`;
            }

        } else {

            let validResult = entities[req.entity][attr].validation(object[attr]);

            if (!validResult) {

                // Add an error message - attribute should be validate 
                errors[attr] = entities[req.entity][attr]["msg"];
            }
        }
    }

    return errors;
}

module.exports = { bodyNotArray, bodyMatchesValidators,validateIdAvailable };