// Data consts
const {validationMethods} = require('./generalValidations.js');
const dataFormat = "json";
const dataPath = "../data/";
// { entity: {entityProperty : matchingValidation}}
const entities = {
    "factories": {

        "address": validationMethods.isAlphaNumeric,
        "name": validationMethods.isAlpha

    }, "bottles": {

        "factory": validationMethods.isExistingEntity("factories"),
        "type": validationMethods.isExistingEntity("types"),
        "manufactoringDate": validationMethods.isDate,
        "expiryDate": validationMethods.isDate

    }, "contacts": {

        "name": validationMethods.isAlpha,
        "address": validationMethods.isAlphaNumeric,
        "phone": validationMethods.isNumeric

    }, "orders": {

        "store": validationMethods.isExistingEntity("stores"),
        "sum": validationMethods.isNumericFloat,
        "date": validationMethods.isDate,
        "bottles": validationMethods.isExistingEntity("bottles")

    }, "stores": {

        "address": validationMethods.isAlphaNumeric,
        "name": validationMethods.isAlpha,
        "ownerId": validationMethods.isExistingEntity("contacts")

    }, "types": {

        "volume": validationMethods.isNumericFloat,
        "price": validationMethods.isNumericFloat
    }
};

// Fill data
const data = Object.keys(entities).reduce((prev, entity) => {
    let path = dataPath + entity + "." + dataFormat;

    return Object.assign({}, prev, { [entity]: require(path) });
}, {});



module.exports = { dataFormat, dataPath, entities, data };