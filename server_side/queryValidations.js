const { validationMethods } = require('./generalValidations.js');
const { entities } = require('./config');

// Query error response example :
/* {
    "location": "query",
    "err": {
        "offset": "Field must be a number, can't be float",
        "limit" : "Field must be a number, can't be float"
        "fields": {
            "field1": "Field field1 doesn't exist for entity contacts",
            "field2": "Field field2 doesn't exist for entity contacts"
        }
    }
}*/

function validateNumeric(property) {

    if (!validationMethods.isNumeric.validation(property) && property !== undefined) {

        return validationMethods.isNumeric.msg;
    }
}

function validateFieldsExist(entity, fields) {
    fields = (fields === undefined) ? undefined : fields.split(",");

    if (fields !== undefined) {

        let fieldsErr = fields.reduce((prev, field) => {
            return (entities[entity][field] === undefined && field !== 'id') ?
                Object.assign({}, prev,
                    { [field]: `Field ${field} doesn't exist for entity ${entity}` }) :
                prev;
        }, {});

        if (Object.keys(fieldsErr).length !== 0) {

            return fieldsErr;
        }
    }
}

function validateQuery(req, res, next) {

    let errors = {};
    errors.location = 'query';
    errors.err = {};

    let { offset, limit, fields } = req.query;
    offset = (offset === undefined) ? 0 : offset;

    // check that offset is not undefined
    // offset and limit only work in /entity (not /entity/:id)
    if (req.params.id == undefined && validateNumeric(offset) !== undefined) {
        errors.err.offset = validateNumeric(offset)
    }
    // Check that limit is not undefined
    if (req.params.id == undefined && validateNumeric(limit) !== undefined) {
        errors.err.limit = validateNumeric(limit);
    } 
    /*************/
    if (validateFieldsExist(req.entity, fields) !== undefined) {
        errors.err.fields = validateFieldsExist(req.entity, fields);
    }

    if (Object.keys(errors.err).length !== 0) {

        res.status(400).send(errors);
    } else {
        next();
    }
}

module.exports = { validateQuery };
