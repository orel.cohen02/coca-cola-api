const validationMethods = { // TODO joi
    isAlphaNumeric: {
        validation: a => (typeof a === "string" && /^[A-Za-z]+[A-Za-z0-9 ]*$/.test(a)),
        msg: "Field must be an alphanumeric string that starts with a letter"
    },
    isAlpha: {
        validation: a => (typeof a === "string" && /^[A-Za-z ]+$/.test(a)),
        msg: "Field must be an alpha string"
    },
    isNumeric: {
        validation: a => /^[0-9]+$/.test(a),
        msg: "Field must be a number, can't be float"
    },
    isNumericFloat: {
        validation: a => /^[0-9]+[.]?[0-9]*$/.test(a),
        msg: "Field must be a number, can be float"
    },
    isExistingEntity: entityName => {
        return {
            validation: id => ((data[entityName].find(x => x.id == id)) !== undefined),
            msg: `No ${entityName} found with this id`
        }
    },
    isDate: {
        validation: a => /^(19|20)\d\d[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])$/.test(a),
        message: "Field must be a date in a format of yyyy-mm-dd"
    }
}

module.exports = {validationMethods};