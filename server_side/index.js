"use strict"

// Express main app consts
const express = require('express');
const app = express();
const {dataFormat} = require('./config.js');
const port = 3000;

const httpStatusCodes = require("http-status-codes");

/************* Parsing each request body before each request handler  *************/

// Middleware to parse body before each request handler
const bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// Make sure content type is application/dataFormat
app.use((req, res, next) => {
    if (req.headers['content-type'] !== `application/${dataFormat}` && (req.method == 'PUT' || req.method == 'POST')) {

        res.status(httpStatusCodes.BAD_REQUEST)
            .send(`Content type must be application/${dataFormat}`);
    } else {
        next();
    }
});

// Handle req.body json syntax errors
app.use((err, req, res, next) => {
    if (err instanceof SyntaxError) {
        res.status(httpStatusCodes.BAD_REQUEST)
            .send(`Invalid ${dataFormat} syntax.`);
    }
});

// Main app uses 
const apiRouter = require('./apiRouter.js');
app.use('/api/v1', apiRouter);
app.listen(port, () => console.log(`Server is listening on port ${port}`));