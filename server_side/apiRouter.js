const express = require('express');
const apiRouter = express.Router();
const httpStatusCodes = require("http-status-codes");
const { entities, data } = require('./config.js');
const { bodyNotArray, bodyMatchesValidators, validateIdAvailable } = require('./bodyValidations.js');
const DAL = new (require('./dal.js'))();

// paths for /entity
const entityPaths = Object.keys(entities).reduce((prev, entity) => prev.concat("/" + entity), []);
// paths for /entity/:id
const entityIdPaths = Object.keys(entities).reduce((prev, entity) => prev.concat("/" + entity + "/:id"), []);
/****************** Setup functions ******************/

// Generate new Ids for entities
function* generateIds(initial) {
    while (true) {
        yield initial++;
    }
}

// Setup Id generator for each entity
const idGenerators = Object.keys(entities).reduce((prev, entity) => {

    return Object.assign({}, prev, { [entity]: generateIds(Math.max.apply(Math, data[entity].map((elem) => elem.id)) + 1) })
}, {});

// General function
function newObject(entity, obj) {

    let newObj = { id: idGenerators[entity].next().value };

    for (let property in entities[entity]) {

        newObj[property] = obj[property];
    }

    return newObj;
}

/*************** API Router Middlewares and routes ***************/

// Middleware to check if the url refers to an entity or not
apiRouter.use((req, res, next) => {

    let entity = req.path.split('/')[1];
    if (Object.keys(entities).indexOf(entity) == -1) {
        res.status(httpStatusCodes.NOT_FOUND).send("No such entity found");
    } else {
        req.entity = entity;
        next();
    }
});

const {validateQuery} = require('./queryValidations.js');
const queryMiddlewares = [
    validateQuery,
    (req, res, next) => {

        req.params.id === undefined ?
            res.data = DAL.getData(req.entity) : // /entity
            res.data = DAL.getObject(req.entity, req.params.id); // /entity/:id

        res.err = {};

        next();
    },
    // TODO complete
    (req, res, next) => {
        if(req.params.id !== undefined) {
            next();
        }
        let { offset, limit} = req.query;
        offset = (offset === undefined) ? 1 : parseInt(offset);
        limit = (limit === undefined) ? data[req.entity].length + 1 - offset : parseInt(limit); // ids start from 1 not 0
        // Assuming the array is sorted
        res.data = res.data.slice(offset - 1, limit + offset);
        next();
    },
    (req, res, next) => {

        let fields = req.query.fields;
        fields = (fields === undefined) ? undefined : fields.split(",");

        if (fields !== undefined) {

                // Data is array
                if (Array.isArray(res.data)) {

                    res.data = res.data.map(obj => fields.reduce((prev, field) =>
                        Object.assign({}, prev, { [field]: obj[field] }), {}));
                } else {
                    res.data = fields.reduce((prev, field) => Object.assign({}, prev, { [field]: res.data[field] }), {});
                }
        }

        next();
    }
    /*(err, req, res, next) => {
        res.status(httpStatusCodes.BAD_REQUEST).send('Query propery not found');
    }*/
];

// ************** FUNCTIONS FOR /entity ************** //

//const entityPaths = /^\/[^/]*[/]?$/;

// GET /entity (with regexp)
// res.err represents query errors
// res.data represents the data requested
// both of them are defined in the query middlewares
apiRouter.get(entityPaths, queryMiddlewares,
    (req, res, next) => {
        (Object.keys(res.err).length === 0) ?
            res.status(200).send(res.data) : res.status(400).send(res.err);
    });

// POST /entity
apiRouter.post(entityPaths, bodyMatchesValidators, (req, res, next) => {

    Array.isArray(req.body) ?
        req.body.map(obj => { DAL.addObject(req.entity, newObject(req.entity, obj)); }) :
        DAL.addObject(req.entity, newObject(req.entity, req.body))

    res.status(httpStatusCodes.OK).send(`${req.entity} were successfully added!`);
});

// PUT /entity
apiRouter.put(entityPaths, bodyMatchesValidators, (req, res, next) => {

    Array.isArray(req.body) ?
        req.body.map(obj => { DAL.updateObject(req.entity, obj.id, obj); }) :
        DAL.updateObject(req.entity, req.body.id, req.body);

    res.status(httpStatusCodes.OK).send(`${req.entity} were successfully updated!`);
});

// DELETE /entity
apiRouter.delete(entityPaths, (req, res, next) => {

    idGenerators[req.entity] = generateIds(1);
    DAL.deleteEntityData(req.entity);
    res.status(200).send(`All ${req.entity} were deleted.`);
});

// ************ FUNCTIONS FOR /entity/:id ************** //

// GET /entity/:id
apiRouter.get(entityPaths, queryMiddlewares,
    (req, res, next) => {
        (Object.keys(res.err).length === 0) ?
            res.status(200).send(res.data) : res.status(400).send(res.err);
    });
apiRouter.get(entityIdPaths, validateIdAvailable, queryMiddlewares, (req, res, next) => {
    (Object.keys(res.err).length === 0) ?
        res.status(200).send(res.data) : res.status(400).send(res.err);
});

// POST /entity/:id
apiRouter.post(entityIdPaths, validateIdAvailable,
    (req, res, next) => {
        res.status(httpStatusCodes.METHOD_NOT_ALLOWED)
            .send(`Method not allowed. Use /${req.entity} to add post new ${req.entity}`);
    });

// PUT /entity/:id
apiRouter.put(entityIdPaths, validateIdAvailable, bodyNotArray, bodyMatchesValidators, (req, res, next) => {

    // Body matches validators puts the body in an array
    req.body = Array.isArray(req.body) ? req.body[0]:req.body;
    DAL.updateObject(req.entity, req.params.id, req.body);

    res.status(httpStatusCodes.OK)
        .send(`${req.entity} with id ${req.params.id} were succesfully updated`);
});

// DELETE /entity/:id
apiRouter.delete(entityIdPaths, validateIdAvailable, (req, res, next) => {

    DAL.deleteObject(req.entity, req.params.id);
    res.status(200).send(`${req.entity} with id ${req.params.id} were deleted`);
});

/***************************************************************/

module.exports = apiRouter;